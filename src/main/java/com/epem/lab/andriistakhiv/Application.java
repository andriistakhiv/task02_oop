package com.epem.lab.andriistakhiv;

import com.epem.lab.andriistakhiv.view.MyView;

public class Application {

  public static void main(String[] args) {
    new MyView().show();
  }
}
