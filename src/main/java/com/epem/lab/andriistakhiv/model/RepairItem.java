package com.epem.lab.andriistakhiv.model;

public abstract class RepairItem {
    private String name;
    private double price;
    private String currency;
    protected Category category;

    public RepairItem(String name, double price, String currency) {
        this.setName(name);
        this.setPrice(price);
        this.setCurrency(currency);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Category getCategory() {
        return category;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @Override
    public String toString() {
        return "\nCategory: " + getCategory() + "\nName: " + getName() + "\nPrice: " + getPrice() + getCurrency();
    }
}