package com.epem.lab.andriistakhiv.model;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import static com.epem.lab.andriistakhiv.constants.Constants.HRYVNIA_SIGN;

public class Domain {
    private List<RepairItem> repairItemList;

    public Domain() {
        generateRepairItemList();
    }

    private void generateRepairItemList() {
        repairItemList = new LinkedList<>();
        repairItemList.add(new Instrument("Screwdriver", 2399.99, HRYVNIA_SIGN));
        repairItemList.add(new Instrument("Drill", 2999.99, HRYVNIA_SIGN));
        repairItemList.add(new Instrument("Perforator", 4999.99, HRYVNIA_SIGN));
        repairItemList.add(new Instrument("Electric fret saw", 3999.99, HRYVNIA_SIGN));
        repairItemList.add(new Instrument("Circular saw", 5099.99, HRYVNIA_SIGN));
        repairItemList.add(new Instrument("Set of hand tools #1", 1099.99, HRYVNIA_SIGN));
        repairItemList.add(new Instrument("Set of hand tools #2", 1599.99, HRYVNIA_SIGN));
        repairItemList.add(new Material("White paint", 299.99, HRYVNIA_SIGN));
        repairItemList.add(new Material("Blue paint", 299.99, HRYVNIA_SIGN));
        repairItemList.add(new Material("Yellow paint", 299.99, HRYVNIA_SIGN));
        repairItemList.add(new Material("Matt lacquer", 399.99, HRYVNIA_SIGN));
        repairItemList.add(new Material("Glossy lacquer", 399.99, HRYVNIA_SIGN));
        repairItemList.add(new WoodenProducts("Door #1", 6009.99, HRYVNIA_SIGN));
        repairItemList.add(new WoodenProducts("Door #2", 4999.99, HRYVNIA_SIGN));
        repairItemList.add(new WoodenProducts("Window #1", 2199.99, HRYVNIA_SIGN));
        repairItemList.add(new WoodenProducts("Window #2", 3199.99, HRYVNIA_SIGN));
        repairItemList.add(new Plumbing("Washbasin #1", 5100.00, HRYVNIA_SIGN));
        repairItemList.add(new Plumbing("Washbasin #2", 4599.99, HRYVNIA_SIGN));
        repairItemList.add(new Plumbing("Urinal", 3000.00, HRYVNIA_SIGN));
        repairItemList.add(new Plumbing("Toilet", 3499.99, HRYVNIA_SIGN));
    }

    public List<RepairItem> sortRepairItemListByPrices() {
        generateRepairItemList();
        repairItemList.sort(Comparator.comparing(RepairItem::getPrice));
        return repairItemList;
    }

    public List<RepairItem> sortRepairItemListByCategory() {
        generateRepairItemList();
        repairItemList.sort(Comparator.comparing(RepairItem::getCategory));
        return repairItemList;
    }

    public List<RepairItem> getRepairItemListByCategory(String category) {
        generateRepairItemList();
        List<RepairItem> categoryList = new LinkedList<>();
        for (RepairItem ri : repairItemList) {
            if (ri.getCategory().toString().equals(category)) {
                categoryList.add(ri);
            }
        }
        repairItemList = categoryList;
        return repairItemList;
    }

    public List<RepairItem> getRepairItemCheaperThen(double price) {
        generateRepairItemList();
        List<RepairItem> priceList = new LinkedList<>();
        for (RepairItem ri : repairItemList) {
            if (ri.getPrice() < price) {
                priceList.add(ri);
            }
        }
        repairItemList = priceList;
        return repairItemList;
    }

    public List<RepairItem> getAllRepairItems() {
        generateRepairItemList();
        return repairItemList;
    }
}