package com.epem.lab.andriistakhiv.model;

import java.util.List;

public interface Model {
    List<RepairItem> getSortedRepairItemListByPrices();

    List<RepairItem> getSortedRepairItemListByCategory();

    List<RepairItem> getRepairItemListByCategory(String category);

    List<RepairItem> getRepairItemCheaperThen(double price);

    List<RepairItem> getAllRepairItems();
}