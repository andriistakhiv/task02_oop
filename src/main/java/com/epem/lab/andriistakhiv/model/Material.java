package com.epem.lab.andriistakhiv.model;

public class Material extends RepairItem {
    public Material(String name, double price, String currency) {
        super(name, price, currency);
        this.category = Category.MATERIAL;
    }
}