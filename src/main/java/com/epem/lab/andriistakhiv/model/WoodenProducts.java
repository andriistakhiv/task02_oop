package com.epem.lab.andriistakhiv.model;

public class WoodenProducts extends RepairItem {
    public WoodenProducts(String name, double price, String currency) {
        super(name, price, currency);
        this.category = Category.WOODEN_PRODUCT;
    }
}