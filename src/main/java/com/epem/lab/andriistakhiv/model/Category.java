package com.epem.lab.andriistakhiv.model;

public enum Category {
    WOODEN_PRODUCT, INSTRUMENT, PLUMBING, MATERIAL;
}