package com.epem.lab.andriistakhiv.model;

public class Instrument extends RepairItem {
    public Instrument(String name, double price, String currency) {
        super(name, price, currency);
        this.category = Category.INSTRUMENT;
    }
}