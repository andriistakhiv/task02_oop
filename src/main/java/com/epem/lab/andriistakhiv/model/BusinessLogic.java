package com.epem.lab.andriistakhiv.model;

import java.util.List;

public class BusinessLogic implements Model {
    private Domain domain;

    public BusinessLogic() {
        domain = new Domain();
    }

    @Override
    public List<RepairItem> getSortedRepairItemListByPrices() {
        return domain.sortRepairItemListByPrices();
    }

    @Override
    public List<RepairItem> getSortedRepairItemListByCategory() {
        return domain.sortRepairItemListByCategory();
    }

    @Override
    public List<RepairItem> getRepairItemListByCategory(String category) {
        return domain.getRepairItemListByCategory(category);
    }

    @Override
    public List<RepairItem> getRepairItemCheaperThen(double price) {
        return domain.getRepairItemCheaperThen(price);
    }

    @Override
    public List<RepairItem> getAllRepairItems() {
        return domain.getAllRepairItems();
    }
}