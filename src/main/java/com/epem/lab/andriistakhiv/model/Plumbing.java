package com.epem.lab.andriistakhiv.model;

public class Plumbing extends RepairItem {
    public Plumbing(String name, double price, String currency) {
        super(name, price, currency);
        this.category = Category.PLUMBING;
    }
}