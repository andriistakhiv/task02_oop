package com.epem.lab.andriistakhiv.controller;

import com.epem.lab.andriistakhiv.model.BusinessLogic;
import com.epem.lab.andriistakhiv.model.RepairItem;
import com.epem.lab.andriistakhiv.model.Model;

import java.util.List;

public class ControllerImpl implements Controller {
    private Model model;

    public ControllerImpl() {
        model = new BusinessLogic();
    }

    @Override
    public List<RepairItem> getSortedRepairItemListByPrices() {
        return model.getSortedRepairItemListByPrices();
    }

    @Override
    public List<RepairItem> getSortedRepairItemListByCategory() {
        return model.getSortedRepairItemListByCategory();
    }

    @Override
    public List<RepairItem> getRepairItemListByCategory(String category) {
        return model.getRepairItemListByCategory(category);
    }

    @Override
    public List<RepairItem> getRepairItemCheaperThen(double price) {
        return model.getRepairItemCheaperThen(price);
    }

    @Override
    public List<RepairItem> getAllRepairItems() {
        return model.getAllRepairItems();
    }
}