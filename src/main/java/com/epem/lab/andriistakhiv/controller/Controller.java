package com.epem.lab.andriistakhiv.controller;

import com.epem.lab.andriistakhiv.model.RepairItem;

import java.util.List;

public interface Controller {
    List<RepairItem> getSortedRepairItemListByPrices();

    List<RepairItem> getSortedRepairItemListByCategory();

    List<RepairItem> getRepairItemListByCategory(String category);

    List<RepairItem> getRepairItemCheaperThen(double price);

    List<RepairItem> getAllRepairItems();
}