package com.epem.lab.andriistakhiv.view;

@FunctionalInterface
public interface Printable {

    void print();
}