package com.epem.lab.andriistakhiv.view;

import com.epem.lab.andriistakhiv.controller.Controller;
import com.epem.lab.andriistakhiv.controller.ControllerImpl;
import com.epem.lab.andriistakhiv.model.RepairItem;

import java.util.*;

import static com.epem.lab.andriistakhiv.constants.Constants.*;

public class MyView {

    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private Map<String, String> categoriesMenu;
    private static Scanner input = new Scanner(System.in);

    public MyView() {
        controller = new ControllerImpl();
        menu = new LinkedHashMap<>();
        menu.put("1", MENU_BUTTON_1);
        menu.put("2", MENU_BUTTON_2);
        menu.put("3", MENU_BUTTON_3);
        menu.put("4", MENU_BUTTON_4);
        menu.put("5", MENU_BUTTON_5);
        menu.put("Q", MENU_BUTTON_EXIT);

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        methodsMenu.put("5", this::pressButton5);

        categoriesMenu = new LinkedHashMap<>();
        categoriesMenu.put("1", WOODEN_PRODUCT_CATEGORY);
        categoriesMenu.put("2", INSTRUMENT_CATEGORY);
        categoriesMenu.put("3", PLUMBING_CATEGORY);
        categoriesMenu.put("4", MATERIAL_CATEGORY);
    }

    private void pressButton1() {
        for (RepairItem ri : controller.getAllRepairItems()) {
            System.out.println(ri);
        }
    }

    private void pressButton2() {
        System.out.println(CATEGORY_MENU);
        outputCategoryMenu();
        showCategoryMenuResult();

    }

    private void pressButton3() {
        System.out.println(CHEAPER_GOODS);
        double price = input.nextDouble();
        for (RepairItem ri : controller.getRepairItemCheaperThen(price)) {
            System.out.println(ri);
        }
    }

    private void pressButton4() {
        for (RepairItem ri : controller.getSortedRepairItemListByPrices()) {
            System.out.println(ri);
        }
    }

    private void pressButton5() {
        for (RepairItem ri : controller.getSortedRepairItemListByCategory()) {
            System.out.println(ri);
        }

    }

    //-------------------------------------------------------------------------

    private void outputCategoryMenu() {
        System.out.println(CATEGORY_MENU);
        for (String str : categoriesMenu.values()) {
            System.out.println(str);
        }
    }

    private void showCategoryMenuResult() {
        String keyCategoryMenu;
        keyCategoryMenu = input.nextLine();
        for (RepairItem ri : controller.getRepairItemListByCategory(categoriesMenu.get(keyCategoryMenu).substring(4)
                .toUpperCase().replace(" ", "_"))) {
            System.out.println(ri);
        }
    }

    private void outputMenu() {
        System.out.println(MENU);
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println(MENU_POINT);
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } while (!keyMenu.equals("Q"));
    }
}