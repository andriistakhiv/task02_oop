package com.epem.lab.andriistakhiv.constants;

public class Constants {
    public static final String MENU_BUTTON_1 = "1 - print all goods list";
    public static final String MENU_BUTTON_2 = "2 - print all goods from some category";
    public static final String MENU_BUTTON_3 = "3 - print all goods with lower price than client will enter";
    public static final String MENU_BUTTON_4 = "4 - sort goods by price";
    public static final String MENU_BUTTON_5 = "5 - sort goods by category";
    public static final String MENU_BUTTON_EXIT = "Q - exit";
    public static final String WOODEN_PRODUCT_CATEGORY = "1 - Wooden Product";
    public static final String INSTRUMENT_CATEGORY = "2 - Instrument";
    public static final String PLUMBING_CATEGORY = "3 - Plumbing";
    public static final String MATERIAL_CATEGORY = "4 - Material";
    public static final String MENU_POINT = "Please, select menu point";
    public static final String CATEGORY_MENU = "\nPlease select a category:";
    public static final String CHEAPER_GOODS = "\nEnter the price you want to see below";
    public static final String HRYVNIA_SIGN = "₴";
    public static final String MENU = "\nMENU:";
}